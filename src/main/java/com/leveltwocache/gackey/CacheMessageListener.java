/**
 * title: CacheMessageListener.java
 * copyleft:©2020-2021 gackey. All Rights Reserved.
 */
package com.leveltwocache.gackey;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @desc TODO
 * @className CacheMessageListener
 * @author gackey
 * @date 2020-11-28 12:09
 */
public class CacheMessageListener implements MessageListener {

    private RedisTemplate<Object, Object> redisTemplate;

    @Autowired
    private RedisCaffeineCacheManager redisCaffeineCacheManager;

    public CacheMessageListener(RedisTemplate<Object, Object> redisTemplate,
        RedisCaffeineCacheManager redisCaffeineCacheManager) {
        super();
        this.redisTemplate = redisTemplate;
        this.redisCaffeineCacheManager = redisCaffeineCacheManager;
    }

    @Override
    public void onMessage(Message message, byte[] pattern) {
        CacheMessage cacheMessage = (CacheMessage) redisTemplate.getValueSerializer().deserialize(message.getBody());
        if (message == null)
            return;
        String cacheName = Optional.ofNullable(cacheMessage.getCacheName()).isPresent() ? cacheMessage.getCacheName()
            : "";
        Object keyName = Optional.ofNullable(cacheMessage.getKey()).isPresent() ? cacheMessage.getKey() : "";
        redisCaffeineCacheManager.clearLocal(cacheName, keyName);
    }

}
