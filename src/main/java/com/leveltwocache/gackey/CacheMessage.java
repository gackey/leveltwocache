/**
 * title: CacheMessage.java
 * copyleft:©2020-2021 gackey. All Rights Reserved.
 */
package com.leveltwocache.gackey;

import java.io.Serializable;

/**
 * @desc TODO
 * @className CacheMessage
 * @author gackey
 * @date 2020-11-28 12:07
 */
public class CacheMessage implements Serializable {

    private String cacheName;

    private Object key;

    public CacheMessage() {
        super();
    }

    public CacheMessage(String cacheName, Object key) {
        super();
        this.cacheName = cacheName;
        this.key = key;
    }

    public String getCacheName() {
        return cacheName;
    }

    public void setCacheName(String cacheName) {
        this.cacheName = cacheName;
    }

    public Object getKey() {
        return key;
    }

    public void setKey(Object key) {
        this.key = key;
    }

}
