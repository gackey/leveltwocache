/**
 * title: RedisCaffeineCache.java
 * copyleft:©2020-2021 gackey. All Rights Reserved.
 */
package com.leveltwocache.gackey;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import org.springframework.cache.support.AbstractValueAdaptingCache;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import com.github.benmanes.caffeine.cache.Cache;

/**
 * @desc TODO
 * @className RedisCaffeineCache
 * @author gackey
 * @date 2020-11-28 12:19
 */
public class RedisCaffeineCache extends AbstractValueAdaptingCache {

    private String name;

    private RedisTemplate<Object, Object> redisTemplate;

    private String cachePrefix;

    private long defaultExpiration = 0;

    private Map<String, Long> expires;

    private String topic = "cache:redis:caffeine:topic";

    private Map<String, ReentrantLock> keyLockMap = new ConcurrentHashMap<>();

    private Cache<Object, Object> caffeineCache;

    protected RedisCaffeineCache(boolean allowNullValues) {
        super(allowNullValues);
    }

    public RedisCaffeineCache(String name, RedisTemplate<Object, Object> redisTemplate,
        Cache<Object, Object> caffeineCache, CacheRedisCaffeineProperties properties) {
        super(properties.isCacheNullValues());
        this.name = name;
        this.redisTemplate = redisTemplate;
        this.caffeineCache = caffeineCache;
        cachePrefix = properties.getCachePrefix();
        this.defaultExpiration = properties.getRedis().getDefaultExpiration();
        this.expires = properties.getRedis().getExpires();
        this.topic = properties.getRedis().getTopic();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Object getNativeCache() {
        return this;
    }

    @Override
    public <T> T get(Object key, Callable<T> valueLoader) {
        Object value = lookup(key);
        if (value != null) {
            return (T) value;
        }
        ReentrantLock lock = keyLockMap.get(key.toString());
        if (lock == null) {
            lock = new ReentrantLock();
            keyLockMap.putIfAbsent(key.toString(), lock);
        }
        try {
            value = lookup(key);
            if (value != null) {
                return (T) value;
            }
            value = valueLoader.call();
            put(key, value);
            return (T) value;
        } catch (Exception e) {
            throw new ValueRetrievalException(key, valueLoader, e.getCause());
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void put(Object key, Object value) {
        if (!super.isAllowNullValues() && value == null) {
            evict(key);
            return;
        }
        Object storeValue = toStoreValue(value);
        long expire = getExpire();
        if (expire > 0) {
            redisTemplate.opsForValue().set(getKey(key), storeValue, expire, TimeUnit.MILLISECONDS);
        } else {
            redisTemplate.opsForValue().set(getKey(key), storeValue);
        }
        push(new CacheMessage(name, key));
        caffeineCache.put(key, storeValue);
    }

    public ValueWrapper putIfAbsent(Object key, Object value) {
        Object cacheKey = getKey(key);
        Object prevValue = null;
        synchronized (key) {
            prevValue = redisTemplate.opsForValue().get(cacheKey);
            if (prevValue == null) {
                long expire = getExpire();
                if (expire > 0) {
                    redisTemplate.opsForValue().set(getKey(key), toStoreValue(value), expire, TimeUnit.MILLISECONDS);
                } else {
                    redisTemplate.opsForValue().set(getKey(key), toStoreValue(value));
                }
                push(new CacheMessage(name, key));
                caffeineCache.put(key, toStoreValue(value));
            }
        }
        return toValueWrapper(prevValue);
    }

    @Override
    public void evict(Object key) {
        redisTemplate.delete(getKey(key));
        push(new CacheMessage(name, key));
        caffeineCache.invalidate(key);
    }

    @Override
    public void clear() {
        Set<Object> keys = redisTemplate.keys(name.concat(":*"));
        if (Optional.ofNullable(keys).isPresent() && keys.size() > 0) {
            for (Object key : keys) {
                redisTemplate.delete(key);
            }
        }
        push(new CacheMessage());
        caffeineCache.invalidateAll();
    }

    @Override
    protected Object lookup(Object key) {
        Object cacheKey = getKey(key);
        Object value = caffeineCache.getIfPresent(key);
        if (value != null) {
            return value;
        }
        value = redisTemplate.opsForValue().get(cacheKey);
        if (value != null) {
            caffeineCache.put(key, value);
        }
        return value;
    }

    private Object getKey(Object key) {
        return name.concat(":")
            .concat(StringUtils.isEmpty(cachePrefix) ? key.toString() : cachePrefix.concat(":").concat(key.toString()));
    }

    private long getExpire() {
        long expire = defaultExpiration;
        Long cacheNameExpire = expires.get(name);
        return cacheNameExpire == null ? expire : cacheNameExpire;
    }

    private void push(CacheMessage message) {
        redisTemplate.convertAndSend(topic, message);
    }

    public void clearLocal(Object key) {
        if (key == null) {
            caffeineCache.invalidateAll();
        } else {
            caffeineCache.invalidate(key);
        }
    }
}
