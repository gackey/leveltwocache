package com.leveltwocache.gackey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication
public class LeveltwocacheApplication {

	public static void main(String[] args) {
		SpringApplication.run(LeveltwocacheApplication.class, args);
	}

}
