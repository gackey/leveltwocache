/**
 * title: RedisCaffeineCacheManage.java
 * copyleft:©2020-2021 gackey. All Rights Reserved.
 */
package com.leveltwocache.gackey;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.cache.CacheManager;
import org.springframework.data.redis.core.RedisTemplate;
import com.github.benmanes.caffeine.cache.Caffeine;

/**
 * @desc TODO
 * @className RedisCaffeineCacheManage
 * @author gackey
 * @date 2020-11-30 11:15
 */
public class RedisCaffeineCacheManager implements CacheManager {

    private ConcurrentMap<String, Cache> cacheMap = new ConcurrentHashMap<>();

    private CacheRedisCaffeineProperties properties;

    private RedisTemplate<Object, Object> redisTemplate;

    private boolean dynamic = true;

    private Set<String> cacheNames;

    public RedisCaffeineCacheManager(CacheRedisCaffeineProperties properties,
        RedisTemplate<Object, Object> redisTemplate) {
        super();
        this.properties = properties;
        this.redisTemplate = redisTemplate;
        this.dynamic = properties.isDynamic();
        this.cacheNames = properties.getCacheNames();
    }

    @Override
    public Cache getCache(String name) {
        Cache cache = cacheMap.get(name);
        if (cache != null) {
            return cache;
        }
        if (!dynamic && cacheNames.contains(name)) {
            return cache;
        }
        cache = new RedisCaffeineCache(name, redisTemplate, caffeineCache(), properties);
        Cache oldCache = (Cache) cache.putIfAbsent(name, cache);
        return oldCache == null ? cache : oldCache;
    }

    public com.github.benmanes.caffeine.cache.Cache<Object, Object> caffeineCache() {
        @NonNull
        Caffeine<Object, Object> cacheBuilder = Caffeine.newBuilder();
        if (properties.getCaffeine().getExpireAfterAccess() > 0) {
            cacheBuilder.expireAfterAccess(properties.getCaffeine().getExpireAfterAccess(), TimeUnit.MILLISECONDS);
        }
        if (properties.getCaffeine().getExpireAfterWrite() > 0) {
            cacheBuilder.expireAfterWrite(properties.getCaffeine().getExpireAfterWrite(), TimeUnit.MILLISECONDS);
        }
        if (properties.getCaffeine().getInitialCapacity() > 0) {
            cacheBuilder.initialCapacity(properties.getCaffeine().getInitialCapacity());
        }
        if (properties.getCaffeine().getMaximumSize() > 0) {
            cacheBuilder.maximumSize(properties.getCaffeine().getMaximumSize());
        }
        if (properties.getCaffeine().getRefreshAfterWrite() > 0) {
            cacheBuilder.refreshAfterWrite(properties.getCaffeine().getRefreshAfterWrite(), TimeUnit.MILLISECONDS);
        }
        return cacheBuilder.build();
    }

    @Override
    public Collection<String> getCacheNames() {
        return this.cacheNames;
    }

    public void clearLocal(String cacheName, Object key) {
        Cache cache = cacheMap.get(cacheName);
        if (cache == null) {
            return;
        }
        RedisCaffeineCache redisCaffeineCache = (RedisCaffeineCache) cache;
        redisCaffeineCache.clearLocal(key);
    }

}
