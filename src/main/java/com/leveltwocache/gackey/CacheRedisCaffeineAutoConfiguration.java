/**
 * title: CacheRedisCaffeineAutoConfiguration.java
 * copyleft:©2020-2021 gackey. All Rights Reserved.
 */
package com.leveltwocache.gackey;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @desc TODO
 * @className CacheRedisCaffeineAutoConfiguration
 * @author gackey
 * @date 2020-11-30 13:32
 */
@Configuration
@AutoConfigureAfter(RedisAutoConfiguration.class)
@EnableConfigurationProperties(CacheRedisCaffeineProperties.class)
public class CacheRedisCaffeineAutoConfiguration {

    @Autowired
    private CacheRedisCaffeineProperties cacheRedisCaffeineProperties;

    @Bean
    @ConditionalOnBean(RedisTemplate.class)
    public RedisCaffeineCacheManager cacheManager(RedisTemplate<Object, Object> redisTemplate) {
        return new RedisCaffeineCacheManager(cacheRedisCaffeineProperties, redisTemplate);
    }

    @Bean
    @ConditionalOnMissingBean(name = "stringKeyRedisTemplate")
    public RedisTemplate<Object, Object> stringKeyRedisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<Object, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory);
        template.setKeySerializer(new StringRedisSerializer());
        template.setHashKeySerializer(new StringRedisSerializer());
        return template;
    }

    @Bean
    public RedisMessageListenerContainer redisMessageListenerContainer(
        RedisTemplate<Object, Object> stringKeyRedisTemplate, RedisCaffeineCacheManager redisCaffeineCacheManager) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        if (stringKeyRedisTemplate == null) {
            throw new NullPointerException("stringKeyRedisTemplate is not exist");
        }
        RedisConnectionFactory connectionFactory = stringKeyRedisTemplate.getConnectionFactory();
        if (connectionFactory == null) {
            throw new NullPointerException("RedisConnectionFactory is not exist");
        }
        container.setConnectionFactory(connectionFactory);
        CacheMessageListener cacheMessageListener = new CacheMessageListener(stringKeyRedisTemplate,
            redisCaffeineCacheManager);
        container.addMessageListener(cacheMessageListener,
            new ChannelTopic(cacheRedisCaffeineProperties.getRedis().getTopic()));
        return container;
    }
}
